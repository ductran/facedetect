# coding: utf-8

import cv2
#import mxnet as mx
#from mtcnn_detector import MtcnnDetector
import os
import time
import numpy as np
import tensorflow as tf
import sys
sys.path.append('.')
import align
from align import detect_face

video_seconds = 0
if len(sys.argv) > 1:
	video_seconds = int(sys.argv[1]);
recording = (video_seconds > 0)

#init tensorflow session
with tf.Graph().as_default():
#with tf.device('/cpu:0'):
	#gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.7)
	#gpu_options.allow_growth = True
	#sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options, log_device_placement=False))
	sess = tf.Session(config=tf.ConfigProto(log_device_placement=False))
	with sess.as_default():
		pnet, rnet, onet = detect_face.create_mtcnn(sess, './align/')
minsize = 20 # minimum size of face
threshold = [0.6, 0.7, 0.8]  # three steps's threshold
factor = 0.709 # scale factor

number_of_frames = 0
t0 = time.time() # start time in seconds
max_frames = 0
if len(sys.argv) > 2:
	camera = cv2.VideoCapture(sys.argv[2])
	if recording:
		_, frame = camera.read() # **EDIT:** to get frame size
		width = np.size(frame, 1) #here is why you need numpy!  (remember to "import numpy as np")
		height = np.size(frame, 0)
		fps=15
		fourcc = cv2.VideoWriter_fourcc('D','I','V','X')
		video_file_name = 'output_'+str(t0)+'.avi'
		out = cv2.VideoWriter(video_file_name,fourcc, fps, (width,height))
		max_frames = fps * video_seconds
else:
	camera = cv2.VideoCapture(-1)
	if recording:
		fps=5
		fourcc = cv2.VideoWriter_fourcc(*'XVID')
		video_file_name = 'output_'+str(t0)+'.avi'
		out = cv2.VideoWriter(video_file_name,fourcc, fps, (640,480))
		max_frames = fps * video_seconds

def showFrame(img, rec):
	cv2.imshow("result", img)
	num_seconds = time.time() - t0 # diff
	recordNext = rec;
	if recordNext:
		if number_of_frames <= max_frames:
			out.write(img)
		else:
			print('Video saved as: ',video_file_name)
			out.release()
			recordNext = False
	if cv2.waitKey(1) & 0xFF == ord('q'):
		return True, recordNext
	return False, recordNext
	
while(camera.isOpened()):
	grab, img = camera.read()
	if img is None:
		continue
	input = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
	
	number_of_frames += 1
	t1 = time.time()
	#results = detector.detect_face(img)
	results, _ = detect_face.detect_face(input, minsize, pnet, rnet, onet, threshold, factor)
	print('time: ',time.time() - t1)
	if results is None:
		stop, recording = showFrame(img, recording);
		if stop:
			break
		continue

	total_boxes = results
	#points = results[1]

	for idx, b in enumerate(total_boxes):
		cv2.rectangle(img, (int(b[0]), int(b[1])), (int(b[2]), int(b[3])), (255, 255, 255),2)
		cv2.putText(img, str(idx+1), (int(b[0])+5, int(b[1])+30), cv2.FONT_HERSHEY_DUPLEX, 1, (255,0,0),1)

	#for p in points:
	#	for i in range(5):
	#		cv2.circle(draw, (p[i], p[i + 5]), 1, (255, 0, 0), 2)
	person = "person"
	number = len(total_boxes)
	if number > 1:
		person = "people"
	cv2.putText(img, str(number) + " " + person + " detected", (10,30), cv2.FONT_HERSHEY_DUPLEX, 1, (0,255,0))
	stop, recording = showFrame(img, recording)
	if stop:
		break
camera.release()
out.release()
cv2.destroyAllWindows()
'''
'''
